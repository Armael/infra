{ config, lib, pkgs, ... }:

let
  tailscaleAddr = "100.64.0.12";
in
{
  imports =
    [
      ./hardware-configuration.nix
      ../common/configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # use the latest kernel compatible with zfs
  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;

  networking.hostName = "mesange"; # Define your hostname.
  networking.hostId = "8425e349";

  users.users.armael = {
    isNormalUser = true;
    extraGroups = [ "wheel" "libvirtd" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEhh0V4CCRKrskoughCzd70KxU+kXRPs9BdATAmPX580 armael@teabox"
    ];
  };

  users.users.lx = {
    isNormalUser = true;
    extraGroups = [ "wheel" "libvirtd" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJpaBZdYxHqMxhv2RExAOa7nkKhPBOHupMP3mYaZ73w9"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIw+IIX8+lZX9RrHAbwi/bncLYStXpI4EmK3AUcqPY2O"
    ];
  };

  services.tailscale = {
    enable = true;
    openFirewall = true;
  };
  systemd.services.tailscaled.serviceConfig.Environment = lib.mkForce [
    "PORT=${toString config.services.tailscale.port}"
    ''"FLAGS=--tun ${lib.escapeShellArg config.services.tailscale.interfaceName} --no-logs-no-support"''
  ];

  # prometheus smartctl exporter
  services.prometheus.exporters.smartctl = {
    enable = true;
    listenAddress = tailscaleAddr;
    openFirewall = true;
    devices = [
      # SSD
      "/dev/disk/by-id/ata-KINGSTON_SA400S37240G_50026B738050A4DA"
      "/dev/disk/by-id/ata-ST4000VN008-2DR166_ZDH2PV07"
      "/dev/disk/by-id/ata-WDC_WD20EZRZ-22Z5HB0_WD-WCC4M1NN1DCL"
    ];
  };
  systemd.services."prometheus-smartctl-exporter".serviceConfig = {
    SupplementaryGroups = [ "qemu-libvirtd" ];
  };

  # Netdata
  # services.netdata.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    # 19999 # Netdata
  ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # disable the setting set by hardware-configuration.nix
  networking.useDHCP = false;
  # setup a fixed IP on the local network + a bridge for the VMs
  systemd.network = {
    enable = true;
    # create the bridge
    netdevs."20-br0" = {
      netdevConfig = {
        Kind = "bridge";
        Name = "br0";
      };
    };
    # assign eno1 to the bridge. eno1 will not take an IP itself, the bridge will
    # the bridge acts like a network switch, all the VMs see the local network
    networks."30-eno1" = {
      matchConfig.Name = "eno1";
      networkConfig.Bridge = "br0";
      linkConfig.RequiredForOnline = "enslaved";
    };
    # configure the bridge
    networks."40-br0" = {
      matchConfig.Name = "br0";
      address = [
        "192.168.1.108/24"
      ];
      routes = [
        {
          routeConfig = {
	    Gateway = "192.168.1.254"; # ip of the box
	    GatewayOnLink = true;
	  };
	}
      ];
      # tells systemd that network is considered up if we can reach the gateway
      linkConfig = {
        RequiredForOnline = "routable";
      };
    };
  };

  networking.nameservers = [
    "192.168.1.254"
  ];

  # set up the hypervisor
  virtualisation.libvirtd = {
    enable = true;
    qemu.runAsRoot = false;
    allowedBridges = [ "br0" ];
    onShutdown = "shutdown";
    parallelShutdown = 10;
  };


  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

