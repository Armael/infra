{ config, lib, pkgs, ... }:

{
  imports = [
    ./secrets-permissions.nix
    ./deploy.nix
  ];

  # Enable the OpenSSH daemon
  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = lib.mkDefault "no";

  services.fail2ban = {
    enable = true;
    maxretry = 5;
    ignoreIP = [
      "neptune.site.deuxfleurs.fr"
    ];
  };

  # Activate nix flakes.
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Use Lix
  nix.package = pkgs.lix;

  time.timeZone = "Europe/Paris";

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr-bepo";
  };

  environment.systemPackages = with pkgs; [
    vim
    neovim
    emacs
    wget
    htop
    tmux
    bmon
    nixfmt-classic
    ncdu
    git
    tig
    restic
    dig
  ];

  # required when using kitty
  environment.enableAllTerminfo = true;

  programs.git = {
    enable = true;
    config.user = {
      name = "root";
      email = "root@${config.networking.hostName}";
    };
    config.alias = {
      ci = "commit";
      co = "checkout";
      st = "status";
      br = "branch";
    };
  };

  services.emacs = {
    enable = true;
    defaultEditor = true;
  };
}
