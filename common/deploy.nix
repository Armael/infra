{ config, pkgs, ... }:

let
  deploy = pkgs.writeShellScriptBin "deploy" ''
    cd /etc/nixos
    git pull
    nixos-rebuild switch
  '';

in { environment.systemPackages = [ deploy ]; }