# This module enforces permissions on secrets stored on the machines.

{ config, lib, ... }:

with lib;

let
  cfg = config.custom.secrets;
in
{
  options.custom.secrets = {
    extraCommands = mkOption {
      default = "";
      type = types.lines;
      description = lib.mdDoc "extra commands to populate /etc/secrets";
    };
  };

  config = {
    system.activationScripts."secrets-permissions" = ''
      # Default to restrictive permissions on secrets.
      # Root can alway read/write/traverse directories no matter the permissions
      # set.

      mkdir -p /etc/secrets

      chown --recursive root:root /etc/secrets
      chmod --recursive 600 /etc/secrets

      # Relax permissions on some secrets.

      # The top directory must be readable and traversable by thoses who need to
      # access secrets.
      chmod 755 /etc/secrets
    '' + cfg.extraCommands;
  };
}