{ config, lib, pkgs, ... }:

{
  security.acme = {
    acceptTerms = true;
    defaults.email = "armael@deuxfleurs.fr";
    defaults.dnsProvider = "infomaniak";
    defaults.credentialFiles = {
      "INFOMANIAK_ACCESS_TOKEN_FILE" = "/etc/secrets/acme/infomaniak_access_token";
    };
    # certs."isomorphis.me" = {};
    # certs."mail" = {
    #   domain = "smtp.isomorphis.me";
    #   extraDomainNames = [
    #     "imap.isomorphis.me"
    # 	"smtp.tremeg.net"
    # 	"imap.tremeg.net"
    # 	"smtp.gueneau.me"
    # 	"imap.gueneau.me"
    #   ];
    # };
    certs."pastila.isomorphis.me" = {};
  };
}