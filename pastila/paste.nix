{ config, lib, pkgs, paste-py, ... }:

{
  # import the nixos module exported by the paste-py flake
  imports = [ paste-py.nixosModules.default ];

  custom.paste-py.enable = true;

  services.nginx.enable = true;
  services.nginx.virtualHosts."paste.isomorphis.me" = {
    enableACME = true;
    forceSSL = true;
    locations."/" = {
      proxyPass = "http://127.0.0.1:8888";
    };
  };
}