{ config, lib, pkgs, ... }:

{
  programs.zsh.enable = true;
  programs.fish.enable = true;

  users.users."armael" = {
    isNormalUser = true;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBvPrmxY7H6t4ah7d0e7/Jt9RkrPHI1MREukOSSwaqlp armael@teabox"
    ];
  };

  users.users."brouette" = {
    isNormalUser = true;
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDamkuh7rHjg0CIb4dCgOBQU/52bGfugOaDHuhu9gOkDFKjtsWCdlMFXBYvEsV0bst3gh0ULhXC5C3TEosvrGxw0VX2VoQOF3DXSVpgwWTTgBpQKtV+uOfv1Wl12cpKPLhS90uAFSD1h88yOZtGD7P/TBAy9PMCOLBrcXXwh8/CfqpetY2Q7navh95E+AxMarJ7p6evYjcq+shsvsWBbo9pKJMnOXUc/nMnD/je0HxSlmHPssye1v0zRHFCFpxQ9ulYwn4MRkTK0+aQxcNQFP/l1CqBIhTxTDaNsWDLAxUW7ZLxJbF6ljXT1ZLWz3XeYm/+vGe/iJFSk7j/TtTlMpM3TviViKz5OeqhsqRvTftTycEW5+kmry8hL4gV4BylOc+kgSXAo7jUiWS3o/1OLbpkImI+nzwXd3ToUQ6H4M21kC4PG+yg9xjNbRuCihiaGDuG1/9QU1oLVWkFk/RlhcbYkz/uZ1f35Es+cPkU3dand6msMplMeGpDK2jX6L6fnzk= jledent@GLaDOS"
    ];
  };

  users.users."jiquiame" = {
    isNormalUser = true;
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDGRsJPeBVrLpAMQceGoI+3ar+0Iex+BwT03lA2VHmbZyBzgxQxTuEay6qSHF6ieZrMRZlMkb+sFaAZgEsb7v5E6brM4lP+5L/dRt81iLgyH6Vg8pYoeM4QgBMDMeXzi/VI6s5tWLGs5vbTeAiNGBVHvR4oUD5x89hcGY6/QWyWoqP+T18UtZFS78U+AZUsajIUyWDDIa4ucF7mx1N8VaFG/LshzIeR0bkuI3N2fFF0DWHc87a8FE6bA1EaOc5pX+VSjWnAZ56kC0WeMlY4bWu3L3sDYIp1G0uhPGrtqWAisjngg4f9POc3ZWL5rHzd+eZ4yabMC3VExF/ws4e0F8Rt gikiam@AntArch"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCnboYOOmlwjnJWxcD2+YimUZOJNxqHWu5QbDsxB4kzdB6U9EFVFKQefC9WKSliswtxb9xKMmm4PGS0tVZ6qM8nWFabA8nYxyrx4+eIVO676xk2iLF327aACcCuUiuNT0h5th1I28BIdJWdea1ZZmsmP0C/JM1KzhCVqCBi6LqZvQ57GW654ljIG85+kYX0pJvnjm0xAWeL85VOrJOdvSDqByp0P1ZFNsIEVhIwJiyKD1AX00vzMonM/I13/JxwCYzYmdjIletvEyxM2iDqzdWdeneO6qfOM87GEkupn0fUSeop4pWn7+Hd/DoT3SPRGzgJgsKbeByxqdI1iR5uIn6awPffvwncLg8d12OVU14Cq1J6upOHcPhA2CiP5qyLn8gpjYZ1Zo25PL5EksvWR/TRvCe8B+JDi0ZzOuKIpDlPyjhUH+Ww5ZZ4aygTjql5HvesAfh4uAwutMEs7KK5yeTi418l6GQHPw/VWFZUcMJv9O3QfqCmsv4vPtuOLDYuDzExyJT+TkRjESt2I+VVc73TE0tR3+wTiRzFSUMy2HI3MlaHJK3LSDfB6D6vpdCCYpgvbfx6ve8OnkdC64WtelXNRM4u6CQKcK+NW8JFkfgFqYPNOnTNECqlueL5PqWcaSG5diL+FoEaZrr3Z4lFP6a4dH4I5Q9Mv1e6EV4lIjZnRQ== ircphone"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIn4bm7k01s35DfOhrVOU0OW1v8b6vx7hmcr79za2pfJ peyotl"
    ];
  };

  users.users."ccube" = {
    isNormalUser = true;
    shell = pkgs.fish;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID0YW3oOmr7yNM2h6tq/UuPkxnu2g5WaXMvuz1zl6XtZ herbrand"
    ];
  };
}