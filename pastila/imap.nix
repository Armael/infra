{ config, lib, pkgs, ... }:

{
  services.dovecot2 = {
    enable = true;
    enableLmtp = true;
    sslServerCert = config.security.acme.certs."imap".directory + "/cert.pem";
    sslServerKey = config.security.acme.certs."imap".directory + "/key.pem";

    mailLocation = "/var/vmail/%d/%n/Maildir";
    extraConfig = ''
    # Exim auth
    service auth {
      unix_listener auth-client {
        mode = 0600
	user = ${config.services.exim.user}
      }
    }
    
    passdb {
      driver = passwd-file
      args = username_format=%n /etc/secrets/dovecot/passwd
    }

    userdb {
      driver = static
      args = uid=vmail gid=vmail home=/var/vmail/%d/%n
    }
    '';
  };

  security.acme.certs."imap" = {
    domain = "imap.isomorphis.me";
    extraDomainNames = [
      "imap.tremeg.net"
      "imap.gueneau.me"
    ];
    group = config.services.dovecot2.group;
  };

  # XXX *shrug*
  services.cron = {
    enable = true;
    systemCronJobs = [
      "0 2 * * * root systemctl restart dovecot2.service"
    ];
  };
}