{ config, lib, pkgs, nixpkgs-unstable, ... }:

let
  vars = import ../vars.nix;
in
{
  imports =
    [
      ../common/configuration.nix
      ./hardware-configuration.nix
      ./backups.nix
      ./users.nix
      ./gitolite.nix
      ./letsencrypt.nix
      ./srv.nix
      ./weechat-relay.nix
      ./prosody.nix
      ./paste.nix
      ./smtp.nix
      ./imap.nix
      ./web-misc.nix
      ./miniflux.nix
      ./convos.nix
    ];

  nixpkgs.overlays = [
    (final: _prev: {
       unstable = import nixpkgs-unstable {
         system = final.system;
	 config.allowUnfree = true;
       };
    })
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda";

  # setup automatic snapshots (~short term: retain no more than 1 month)
  services.zfs.autoSnapshot = {
    enable = true;
    frequent = 0;
    monthly = 0;
  };

  # automatic scrub, 10th of each month at 3AM
  services.zfs.autoScrub = {
    enable = true;
    interval = "*-*-10 03:00:00";
  };

  networking.hostName = "pastila"; # Define your hostname.
  networking.hostId = "8425e349";

  networking.useDHCP = false;
  networking.interfaces."enp1s0".ipv4.addresses = [ vars.ovenNat.pastila ];
  networking.defaultGateway = {
    address = vars.ovenNat.oven.address;
    interface = "enp1s0";
  };
  networking.nameservers = vars.onlineNetDNS;

  networking.hosts = {
    "127.0.0.1" = [ "isomorphis.me" ];
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts =
    map (port: port.num)
      (builtins.filter (port: port.proto == "tcp") vars.ovenNat.forwardPorts);
  networking.firewall.allowedUDPPorts =
    map (port: port.num)
      (builtins.filter (port: port.proto == "udp") vars.ovenNat.forwardPorts);

  programs.mosh.enable = true;

  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
    fuse
    fuse3
  ];

  environment.systemPackages = with pkgs; [
    irssi
    weechat
    transmission_3
    tremc
    unstable.poezio
  ];

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

