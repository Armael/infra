{ config, lib, pkgs, ... }:

{
  services.nginx.enable = true;

  services.nginx.virtualHosts."armael.gueneau.me" = {
    globalRedirect = "gallium.inria.fr/~agueneau";
  };

  services.nginx.virtualHosts."gueneau.me" = {
    globalRedirect = "armael.gueneau.me";
  };

}