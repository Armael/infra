{ config, lib, pkgs, ... }:

{
  services.prosody = {
    enable = true;

    # TODO: setup a MUC to be compliant
    xmppComplianceSuite = false;

    uploadHttp = {
      domain = "xu.isomorphis.me";
    };

    ssl = {
      cert = config.security.acme.certs."prosody".directory + "/cert.pem";
      key = config.security.acme.certs."prosody".directory + "/key.pem";
    };

    virtualHosts."isomorphisme" = {
      enabled = true;
      domain = "isomorphis.me";
    };

    admins = [ "armael@isomorphis.me" ];
  };

  security.acme.certs."prosody" = {
    domain = "isomorphis.me";
    extraDomainNames = [
      "xmpp.isomorphis.me"
      "xmppproxy.isomorphis.me"
      "xu.isomorphis.me"
    ];
    group = config.services.prosody.group;
    reloadServices = [ "prosody" ];
  };

}
