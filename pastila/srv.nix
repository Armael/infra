{ config, lib, pkgs, ... }:

let
  up_dir = "/srv/up";
  isomorphisme_dir = "/srv/isomorphis.me";
  tremeg_dir = "/srv/tremeg.net";
  dev_dir = "/srv/dev";
in
{
  services.nginx.enable = true;

  # nginx runs under ProtectHome=true which disallows reading anywhere
  # in /home. So we need to use a different location.
  users.users."up" = {
    isNormalUser = true;
    home = "${up_dir}";
    # group = config.services.nginx.group;
    group = "nginx";
    createHome = true;
    homeMode = "750";
  };

  services.nginx.virtualHosts."srv.isomorphis.me" = {
    forceSSL = true;
    enableACME = true;
    root = "${up_dir}";
    locations = {
      "/" = {
        extraConfig = "autoindex on;";
      };
      "/.ssh" = {
        return = "403";
      };
      "/i/" = {
        extraConfig = "autoindex off;";
      };
    };
  };

  services.nginx.virtualHosts."isomorphis.me" = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      root = "${isomorphisme_dir}";
    };
  };

  services.nginx.virtualHosts."tremeg.net" = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      root = "${tremeg_dir}";
    };
  };

  services.nginx.virtualHosts."dev.isomorphis.me" = {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      root = "${dev_dir}/public";
    };
  };

  system.activationScripts."srv-permissions" = ''
    chown -R up:nginx ${up_dir}
    chown -R nginx:nginx ${isomorphisme_dir}
    chown -R nginx:nginx ${tremeg_dir}
    chown -R nginx:nginx ${dev_dir}
  '';
}