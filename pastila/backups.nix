{ config, lib, pkgs, ... }:

{
  services.restic.backups."infracoll" = {
    paths = [
      "/home"
      "/root"
      "/etc/secrets"
      "/var"
      "/srv"
    ];

    exclude = [
      "/var/cache"
      "/home/*/.cache"
      "/var/log"
      ".opam"
      "/home/armael/TorrentsData"
    ];

    timerConfig = {
      OnCalendar = "daily";
      RandomizedDelaySec = "5h";
      Persistent = true;
    };

    pruneOpts = [
      "--keep-daily 7"
      "--keep-weekly 5"
      "--keep-monthly 6"
      "--keep-yearly 3"
    ];

    repositoryFile = /etc/secrets/restic/repo;
    passwordFile = "/etc/secrets/restic/password";
    environmentFile = "/etc/secrets/restic/env";
  };

}
