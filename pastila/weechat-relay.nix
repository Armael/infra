{ config, lib, pkgs, ... }:

let
  mkProxy = port: {
    forceSSL = true;
    enableACME = true;
    locations."/" = {
      proxyPass = "http://localhost:" + builtins.toString port + "/weechat";
      proxyWebsockets = true;
      extraConfig = ''
        proxy_read_timeout 604800;                # Prevent idle disconnects
      '';
    };
  };
in
{
  services.nginx.enable = true;
  services.nginx.virtualHosts."relayjq.isomorphis.me" = mkProxy 9001;
}