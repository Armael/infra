{ config, lib, pkgs, ... }:

{
  services.miniflux = {
    enable = true;
    adminCredentialsFile = "/etc/secrets/miniflux/admin";
  };

  services.nginx.enable = true;
  services.nginx.virtualHosts."rss.isomorphis.me" = {
    enableACME = true;
    forceSSL = true;
    locations."/" = {
      proxyPass = "http://localhost:8080";
    };
  };
}