{
  ovenNat = {
    oven = { address = "10.0.42.1"; prefixLength = 24; };
    pastila = { address = "10.0.42.100"; prefixLength = 24; };

    # ports to open on pastila and forward through the NAT in oven
    forwardPorts = [
      { num = 80; proto = "tcp"; }
      { num = 443; proto = "tcp"; }
      { num = 22; proto = "tcp"; }
      # XMPP
      { num = 5222; proto = "tcp"; }
      { num = 5222; proto = "udp"; }
      { num = 5269; proto = "tcp"; }
      { num = 5269; proto = "udp"; }
      # Prosody HTTP
      { num = 5280; proto = "tcp"; }
      { num = 5281; proto = "tcp"; }
      # Prosody proxy65
      { num = 5000; proto = "tcp"; }
      # DHT
      { num = 6881; proto = "udp"; }
      # transmission
      { num = 51413; proto = "tcp"; }
      { num = 51413; proto = "udp"; }
      # mosh
      # see ad-hoc config in oven/configuration.nix
      # (60000:61000)
      # Headscale RPC
      { num = 50443; proto = "tcp"; }
      # SMTP
      { num = 25; proto = "tcp"; }
      { num = 465; proto = "tcp"; }
      { num = 587; proto = "tcp"; }
      # IMAP
      { num = 143; proto = "tcp"; }
      { num = 993; proto = "tcp"; }
      # pixels (cours prog2)
      { num = 110; proto = "tcp"; }
    ];
  };

  onlineNetDNS = [
    "51.159.47.28"
    "51.159.47.26"
  ];
}
