# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

let
  vars = import ../vars.nix;
in
{
  imports =
    [
      ./hardware-configuration.nix
      ../common/configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  # serial console
  boot.kernelParams = [ "console=ttyS1" ];

  # network config
  networking = {
    hostName = "oven";

    useDHCP = false;

    # Static public IP, online.net gateway & DNS
    interfaces."enp1s0".ipv4.addresses = [{
      address = "51.15.168.181";
      prefixLength = 24;
    }];
    defaultGateway = {
      address = "51.15.168.1";
      interface = "enp1s0";
    };
    nameservers = vars.onlineNetDNS;

    bridges = {
      "br0" = { interfaces = []; };
    };
    interfaces."br0".ipv4.addresses = [ vars.ovenNat.oven ];

    nat = {
      enable = true;
      internalInterfaces = ["br0"];
      externalInterface = "enp1s0";
      forwardPorts =
        map (port: {
          destination =
            toString vars.ovenNat.pastila.address + ":" + toString port.num;
          proto = port.proto;
          sourcePort = port.num;
        }) vars.ovenNat.forwardPorts
	++
	# mosh
	[{
	  destination = toString vars.ovenNat.pastila.address + ":60000-61000";
	  proto = "udp";
	  sourcePort = "60000:61000";
	}];
    };

    firewall = {
      allowedTCPPorts =
        map (port: port.num)
          (builtins.filter (port: port.proto == "tcp") vars.ovenNat.forwardPorts);
      allowedUDPPorts =
        map (port: port.num)
          (builtins.filter (port: port.proto == "udp") vars.ovenNat.forwardPorts);
      # mosh
      allowedUDPPortRanges = [
        { from = 60000; to = 61000; }
      ];
    };
  };

  users.users.armael = {
    isNormalUser = true;
    extraGroups = [ "wheel" "libvirtd" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDRa9ABfjRX61sygceiefWnyWK0+gZ5YaaCDxUQeE2LT armael@teabox"
    ];
  };

  services.openssh.ports = [ 2222 ];

  # Set up the hypervisor
  virtualisation.libvirtd = {
    enable = true;
    qemu.runAsRoot = false;
    onShutdown = "shutdown";
    parallelShutdown = 10;
  };


  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.05"; # Did you read the comment?

}
