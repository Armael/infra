# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../common/configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda";

  # workaround because we are in a vm
  boot.zfs.devNodes = "/dev/disk/by-partuuid";

  # setup automatic snapshots (~short term: retain no more than 1 month)
  services.zfs.autoSnapshot = {
    enable = true;
    frequent = 0;
    monthly = 0;
  };

  # automatic scrub, 20th of each month at 3AM
  services.zfs.autoScrub = {
    enable = true;
    interval = "*-*-20 03:00:00";
  };

  networking.hostName = "pinson";
  # required by zfs
  networking.hostId = "8425e349";

  services.openssh.settings.PasswordAuthentication = false;
  services.openssh.settings.PermitRootLogin = "yes";

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDiT41LuYEluLxyyWR+zM1cOhcp6cC0DosCi1VivJCv8 armael@teabox"
  ];

  users.users."music" = {
    isNormalUser = true;
    uid = 1001;
  };

  users.users."macaron" = {
    isNormalUser = true;
    uid = 1002;
  };

  users.groups = {
    music = {
      gid = 1001;
      members = [ "music" "macaron" ];
    };
  };

  users.users."macaron".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHrUE40cFRIcnVI8rtopNlC1BVb58jS27/K2PQq5oZPd armael@macaron.isomorphis.me"
  ];

  environment.systemPackages = with pkgs; [
    zfs
    beets
  ];

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

