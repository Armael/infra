{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
  inputs.nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.paste-py.url = "git+https://github.com/Armael/paste-py?rev=27a639f5aaf970b1ccfc1657541abed7a874aea3";
  # inputs.paste-py.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, ... }@inputs: {
    nixosConfigurations."oven" = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = inputs;
      modules = [ ./oven/configuration.nix ];
    };

    nixosConfigurations."pastila" = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = inputs;
      modules = [ ./pastila/configuration.nix ];
    };

    nixosConfigurations."mesange" = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = inputs;
      modules = [ ./mesange/configuration.nix ];
    };

    nixosConfigurations."pinson" = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = inputs;
      modules = [ ./pinson/configuration.nix ];
    };
  };
}
